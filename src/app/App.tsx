import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

// pages
import SiteEditor from "@/app/pages/SiteEditor";

const App: React.FC = () => {
    return (
        <Router>
            <Switch>
                <Route path="/" component={SiteEditor} />
            </Switch>
        </Router>
    );
};

export default App;

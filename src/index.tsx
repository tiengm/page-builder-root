import React from "react";
import ReactDOM from "react-dom";
import reportWebVitals from "./reportWebVitals";

// app
import App from "./app";

// styles
import "./core/styles/index.css";

ReactDOM.render(
    <React.StrictMode>
        <App />
    </React.StrictMode>,
    document.getElementById("root")
);

reportWebVitals();
